CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module allows you to compare the permissions between selected roles.
It will give you the list of not matching permissions as list with module name.


CONFIGURATION
-------------

 * Enable Compare role permissions at admin/modules
 * Visit <site-url>/admin/crp to compare the permissions.


MAINTAINERS
-----------

Current maintainers:
 * visabhishek - https://www.drupal.org/user/896554
 * sriharshauppuluri - https://www.drupal.org/user/1211580
