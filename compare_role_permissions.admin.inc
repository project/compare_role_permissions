<?php

/**
 * @file
 * Admin page callbacks for the compare role permissions module.
 */

/**
 * Implements hook_form().
 */
function compare_role_permissions_form($form, $form_state) {
  $form = array();

  $form['role1'] = array(
    '#type' => 'select',
    '#title' => 'Role',
    '#required' => TRUE,
    '#options' => user_roles(),
    '#default_value' => isset($form_state['values']['role1']) ? $form_state['values']['role1'] : DRUPAL_ANONYMOUS_RID,
  );

  $form['role2'] = array(
    '#type' => 'select',
    '#title' => 'Role',
    '#required' => TRUE,
    '#options' => user_roles(),
    '#default_value' => isset($form_state['values']['role2']) ? $form_state['values']['role2'] : DRUPAL_AUTHENTICATED_RID,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Compare',
  );

  if (!empty($form_state['results_table'])) {
    $form['results_table'] = array('#markup' => $form_state['results_table']);
  }

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function compare_role_permissions_form_validate($form, &$form_state) {
  if ($form_state['values']['role1'] == $form_state['values']['role2']) {
    form_set_error('role2', t('Roles should be different from each other.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function compare_role_permissions_form_submit($form, &$form_state) {
  $all_role = user_roles();
  $role1 = $form_state['values']['role1'];
  $role2 = $form_state['values']['role2'];
  $header = array(
    array('data' => t('Module')),
    array('data' => t('Permission')),
    array('data' => $all_role[$role1]),
    array('data' => $all_role[$role2]),
  );

  $options = array();
  $roleprm1 = compare_role_permissions_get_permission($role1);
  $roleprm2 = compare_role_permissions_get_permission($role2);
  $roleprm1_keys = array_keys($roleprm1);
  $roleprm2_keys = array_keys($roleprm2);
  $all_diff_perm = array_merge(array_diff($roleprm1_keys, $roleprm2_keys), array_diff($roleprm2_keys, $roleprm1_keys));
  if (!empty($all_diff_perm)) {
    for ($i = 0; $i < count($all_diff_perm); $i++) {
      $row = array();
      $module_name = isset($roleprm1[$all_diff_perm[$i]][0]) ? $roleprm1[$all_diff_perm[$i]][0] : $roleprm2[$all_diff_perm[$i]][0];
      $fragment_link = 'module-' . $module_name;
      $row[] = l($module_name, 'admin/people/permissions', array('attributes' => array('target'=>'_blank'), 'fragment' => $fragment_link));
      $row[] = $all_diff_perm[$i];
      $row[] = in_array($all_diff_perm[$i], $roleprm1_keys) ? 'Yes' : 'No';
      $row[] = in_array($all_diff_perm[$i], $roleprm2_keys) ? 'Yes' : 'No';
      $options[] = $row;
    }
    $form_state['results_table'] = theme(
      'table', array('header' => $header, 'rows' => $options)
    );
  }
  else {
    $form_state['results_table'] = 'Both the selected roles have similar permissions';
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Helper function to fetch permissions list that are enabled for the role id.
 *
 * @param int $rid
 *   Role id of role.
 *
 * @return array
 *   Module, permissions list of given role id.
 */
function compare_role_permissions_get_permission($rid) {
  $query = db_select('role_permission', 'rp');
  $query->fields('rp', array('rid', 'module', 'permission'));
  $query->condition('rp.rid', $rid, '=');
  $query->orderBy('rp.module');
  $res = $query->execute()->fetchAll();
  $permissions = array();
  foreach ($res as $values) {
    $permissions[$values->permission] = array($values->module, $values->rid);
  }
  return $permissions;
}
